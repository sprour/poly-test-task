package io.polybius.testtask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class QueryTest {

    String query;
    List<JsonObject> expectedResult;

    public QueryTest(String query, List<JsonObject> expectedResult) {
        this.query = query;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters(name="{0}: {1}")
    public static Collection<Object[]> data(){
        HashMap<String, String> testData = new HashMap<>();
        testData.put("name=Bob", "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400.00}]");
        testData.put("wage=3500", "[{ \"name\":\"Jhon\",\"age\":35, \"wage\":3500.00}]");
        testData.put("age>35", "[{ \"name\":\"Stan\",\"age\":38, \"wage\":3300.00}]");
        testData.put("age<35", "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400.00}]");
        testData.put("wage>=3300", "[{ \"name\":\"Jhon\",\"age\":35, \"wage\":3500.00}, { \"name\":\"Stan\",\"age\":38, \"wage\":3300.00}]");
        testData.put("wage<=3300", "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400.00}, { \"name\":\"Stan\",\"age\":38, \"wage\":3300.00}]");
        testData.put("age>35 && wage<3500 || name=Bob", "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400.00}, { \"name\":\"Stan\",\"age\":38, \"wage\":3300.00}]");
        List<Object[]> data = new ArrayList<>();
        for (Map.Entry<String, String> entry : testData.entrySet()) {
           String query = entry.getKey();
            JsonParser jsonParser = new JsonParser();
            JsonArray jsonArray = jsonParser.parse(entry.getValue()).getAsJsonArray();
            List<JsonObject> jsonObjectList = getJsonObjectList(jsonArray);
            data.add(new Object[]{query, jsonObjectList});
        }
        return data;

    }

    @Test
    public void testQuery() throws Throwable {

        String dataString = "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400.00},{ \"name\":\"Jhon\",\"age\":35, \"wage\":3500.00}, { \"name\":\"Stan\",\"age\":38, \"wage\":3300.00}]";

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(dataString).getAsJsonArray();
        List<JsonObject> jsonObjectsList = new ArrayList<>();
        QueryParser queryParser = new QueryParser(query);
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonElement jsonElement = jsonArray.get(i).getAsJsonObject();
            boolean exists = queryParser.checkElement((JsonObject) jsonElement);
            if (exists) jsonObjectsList.add((JsonObject) jsonElement);
        }
        assertEquals(expectedResult, jsonObjectsList);
    }

    private static List<JsonObject> getJsonObjectList(JsonArray jsonArray){
        List<JsonObject> jsonObjectList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonElement = jsonArray.get(i).getAsJsonObject();
            jsonObjectList.add(jsonElement);
        }
        return  jsonObjectList;
    }

}
