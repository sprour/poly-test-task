package io.polybius.testtask;

public class Statement {

   private String memberName;
   private String memberValue;
   private Operator operator;

   public Statement(String memberName, String memberValue, Operator operator) {
      this.memberName = memberName;
      this.memberValue = memberValue;
      this.operator = operator;
   }

   public String getMemberName() {
      return memberName;
   }

   public void setMemberName(String memberName) {
      this.memberName = memberName;
   }

   public String getMemberValue() {
      return memberValue;
   }

   public void setMemberValue(String memberValue) {
      this.memberValue = memberValue;
   }

   public Operator getOperator() {
      return operator;
   }

   public void setOperator(Operator operator) {
      this.operator = operator;
   }
}
