package io.polybius.testtask;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {

        String query = args[0];
        String dataString = args[1];

        //String query = "name=Bob";
        //query = "age=35";
        //query = "age<=35";
        //query = "age<35 && wage>2100 || age=38 && wage>3000";
        //String dataString = "[{ \"name\":\"Bobby\",\"age\":30, \"wage\":2400},{ \"name\":\"Sergei\",\"age\":35, \"wage\":3000}, { \"name\":\"Dmitri\",\"age\":38, \"wage\":3500}]";

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(dataString).getAsJsonArray();
        List<JsonObject> jsonObjectsList = new ArrayList<>();

        QueryParser parser = new QueryParser(query);

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonElement jsonElement = jsonArray.get(i).getAsJsonObject();
            boolean exists = parser.checkElement((JsonObject) jsonElement);
            if (exists) jsonObjectsList.add((JsonObject) jsonElement);
        }
        List<JsonObject> filteredResult = jsonObjectsList;

        System.out.println(new Gson().toJson(filteredResult));
    }
}
