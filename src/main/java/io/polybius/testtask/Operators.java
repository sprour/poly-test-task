package io.polybius.testtask;

import java.util.HashMap;
import java.util.Map;

public class Operators {
    private final Map<String, Operator> ops = new HashMap<>();
    public Operators() {
        // Operator =
        this.ops.put("=", (a, b) -> a == b);

        // Operator >=
        this.ops.put(">=", (a, b) -> a >= b);

        // Operator <=
        this.ops.put("<=", (a, b) -> a <= b);

        // Operator >
        this.ops.put(">", (a, b) -> a > b);

        // Operator >
        this.ops.put("<", (a, b) -> a < b);
    }

	public Operator get(String operatorKey) {
        return this.ops.get(operatorKey);
    }

    public Map<String, Operator> list() {
        return ops;
    }
}
