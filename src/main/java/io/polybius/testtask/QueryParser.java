package io.polybius.testtask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryParser {

    public String rawQuery;

    private List<List<Statement>> parsedQuery = new ArrayList<>();

    private Map<String, Operator> operators;

    public QueryParser(String rawQuery) {
        this.rawQuery = rawQuery.replaceAll("\\s+", "");
        this.operators = new Operators().list();

        if (this.rawQuery.contains("||")) {
            String[] orArr = this.rawQuery.split("\\|\\|");
            for (String or : orArr) {
                if (or.contains("&&")) {
                    List<Statement> andStatement = getAndStatement(or);
                    parsedQuery.add(andStatement);

                } else { // Single expression
                    Statement statement = getSingleStatement(or);
                    if (statement != null) {
                        List<Statement> statements = new ArrayList<>();
                        statements.add(statement);
                        parsedQuery.add(statements);
                    }
                }
            }
        } else if (this.rawQuery.contains("&&")) {
            List<Statement> statements = getAndStatement(this.rawQuery);
            parsedQuery.add(statements);
        } else {
            Statement statement = getSingleStatement(this.rawQuery);
            List<Statement> statements = new ArrayList<>();
            statements.add(statement);
            parsedQuery.add(statements);
        }

    }

    private Statement getSingleStatement(String q) {
        Statement statement = null;
        Pattern pattern = Pattern.compile("[<>]=?|=");
        Matcher matcher = pattern.matcher(q);
        if(matcher.find()){
            MatchResult result = matcher.toMatchResult();
			String op = result.group();
            Operator operator = operators.get(op);
            if(operator !=null){
                String[] p = q.split(op);
                statement = new Statement(p[0], p[1], operator);
            }
        }
        return statement;
    }

    private List<Statement> getAndStatement(String q1) {
        List<Statement> andStatements = new ArrayList<>();
        String[] andQ = q1.split("&&");
        for (String s : andQ) {
            Statement statement = getSingleStatement(s);
            andStatements.add(statement);
        }
        return andStatements;
    }

    boolean checkElement(JsonObject jsonObject) {

        for (List<Statement> andStatements : this.parsedQuery) {

            List<Boolean> and = new ArrayList<>();
            for (Statement statement : andStatements) {

                String memberName = statement.getMemberName();
                String memberValue = statement.getMemberValue();
                Operator operator = statement.getOperator();

                boolean hasMember = jsonObject.has(memberName);

                if (hasMember) {
                    JsonElement value = jsonObject.get(memberName);
                    if (Utils.isNumber(value.getAsString()) && Utils.isNumber(memberValue)) {
                        double v1 = Double.parseDouble(value.getAsString());
                        double v2 = Double.parseDouble(memberValue);
                        // Compare numbers
                        boolean res = operator.compare(v1, v2);
                        and.add(res);
                    } else {
                        boolean res = value.getAsString().contains(memberValue);
                        and.add(res);
                    }

                } else {
                    and.add(false);
                }
            }

            Boolean isTrueStatement = Utils.isTrue(and);
            if(isTrueStatement) return true;

        }

        return false;

    }




}
