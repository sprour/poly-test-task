package io.polybius.testtask;

public interface Operator {

   boolean compare(double v1, double v2);

}
