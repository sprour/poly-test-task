package io.polybius.testtask;

import java.util.List;

public class Utils {

    static boolean isNumber(String st) {
        if (st.charAt(0) == '-' && st.length() == 1)
            return false;
        if (st.charAt(0) == '+' && st.length() == 1)
            return false;
        if (st.charAt(0) == '.' && (st.length() == 1 || !Character.isDigit(st.charAt(1))))
            return false;
        if (st.charAt(0) == 'e' || st.charAt(0) == 'E')
            return false;
        for (char ch : st.toCharArray()) {
            if (!Character.isDigit(ch) && ch != '-' && ch != '.' && ch != 'e' && ch != 'E'
                    && ch != '+')
                return false;
        }
        return true;
    }

    static boolean isTrue(List<Boolean> booleanArray) {
        boolean allTrue = true;
        for (boolean b : booleanArray) {
            if (!b) {
                allTrue = false;
                break;
            }
        }
        return allTrue;
    }
}
